terraform {
  backend "gcs" {
    bucket      = "uniesp-devops-storage"
    prefix      = "dev/state"
    credentials = "user.json"
  }
}